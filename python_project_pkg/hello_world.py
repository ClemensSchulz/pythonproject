#!/usr/bin/env python
"""This script prompts Hello World."""

def print_me(text):
    """Return printed string."""
    print(text)


if __name__ == "__main__":
    print_me("Hello World")
